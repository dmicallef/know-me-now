FROM tiangolo/uwsgi-nginx-flask:python3.6

ENV FLASK_APP /app/main.py

COPY ./app /app
COPY requirements.txt .

RUN pip install -r requirements.txt
RUN flask db init
RUN flask db migrate
RUN flask db upgrade
