import logging
import random
import ed25519

from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_api import status
from sqlalchemy.exc import IntegrityError

logger = logging.getLogger('')
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)
migrate = Migrate(app, db)


# Models
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    pub_key = db.Column(db.String(32), unique=True, nullable=False)

    def __str__(self):
        return f'{self.username} : {self.email}'


# Utils
def generate_nonce(length=32):
    """Generate pseudorandom number."""
    return ''.join([str(random.randint(0, 9)) for _ in range(length)])


def show_login_qr_code(nonce):

    print(f'Nonce = {nonce}')
    return render_template('login.html', nonce=nonce)


def get_sqrl_items(json_object):
    _encrypted_url = 'encrypted_url'
    _verification_key = 'verification_key'

    encrypted_url = None
    verification_key = None

    if _encrypted_url in json_object.keys():
        encrypted_url = json_object[_encrypted_url]

    if _verification_key in json_object.keys():
        verification_key = json_object[_verification_key]

    return encrypted_url, verification_key


def verify_ed25519(auth, verification_key, msg=None):
    assert isinstance(verification_key, bytes)
    ver_key = ed25519.VerifyingKey(verification_key)
    ver_key.verify(auth, msg)

    # TODO :
    # Save  message URL
    # return verify depending on Message
    return True


def do_qr_code_login(json_object):
    if json_object:
        app.logger.info(f'Json object found. {json_object}')

        auth, verification_key = get_sqrl_items(json_object)

        verified = False
        if auth and verification_key:
            verified = verify_ed25519(auth, verification_key)

        if not verified:
            # todo return status bad request
            return render_template('login.html', verification=verified)

        expected_keys = ['username', 'email', 'pub_key']

        kwargs = dict()
        for key in expected_keys:
            if key in json_object.keys():
                kwargs[key] = json_object[key]

            else:
                # todo : return 200 0k - redirect user
                return render_template('login.html', verification=verified,
                                       message=f'{expected_keys} expected.')

        try:
            user = User(**kwargs)
            users_saved = db.query.filter_by(**kwargs)
            # Todo use .count() sqlalchemy
            if len(users_saved) < 1:
                db.session.add(user)
                db.session.commit()

        except IntegrityError:
            app.logger.warn('Unable to login...')
            return render_template('login.html', verification=verified, message='User already taken')

        return render_template('index.html', username=kwargs['username'])

    else:
        return render_template('login.html')


@app.route("/")
def index():
    nonce = generate_nonce()
    return sign_up(nonce=nonce)


@app.route('/sqrl/?key=<int:nonce>', methods=['GET', 'POST'])
def sign_up(nonce):
    if request.method == 'GET':
        return show_login_qr_code(nonce)
    else:
        if request.is_json:
            return do_qr_code_login(request.json, nonce)
        else:
            # logger.warn
            return 'Object of type "application/json" expected, ' \
                   'with data including encrypted url, and verification key', status.HTTP_400_BAD_REQUEST


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
